from django.shortcuts import render

# Create your views here.
from django import http
from django.views import View

# 测试模板能否正常的运行
# def index(request):
#     context={'city': '北京'}
#     return render(request,'11.html',context)

class Register(View):

    # 展示注册界面
    def get(self,request):

        return render(request,'register.html')

    # 注册功能的实现（业务逻辑）
    # 1. 接收前端表单数据 request.POST.get
    # 2. 前端数据拿到后,在使用之前都要做校验
    # 3. 新增User
    # 4. 响应
    def post(self,request):

        query_dict = request.POST
        username = query_dict.get('username')
        password = query_dict.get('password')
        password2 = query_dict.get('password2')
        mobile = query_dict.get('mobile')
        sms_code = query_dict.get('sms_code')
        allow = query_dict.get('allow')

        # 2. 前端数据拿到后,在使用之前都要做校验
        # all()  遍历列表中每一个元素 判断是否为None '', [], () .. 只要其中有一个是空 直接返回False
        if all([username, password, password2, mobile, sms_code, allow]) is False:
            return http.HttpResponseForbidden('缺少必传参数')

        # 3. 新增User
        # 4. 响应

        pass

